Adaptation of a development by Andrew M. Pitts

```
{-# OPTIONS --cubical #-}

open import Cubical.Core.Everything
open import Cubical.Foundations.Prelude
open import Cubical.HITs.PropositionalTruncation hiding (∣_∣)
open import Data.List                            using (List; []; _∷_; _++_; [_]; map)
open import Data.Product                         using (_,_; _×_; proj₁; proj₂)
open import Cubical.Data.Sigma.Properties        using (Σ≡)
open import Cubical.Foundations.HLevels          using (ΣProp≡; propPi)
open import Cubical.Relation.Nullary.DecidableEq using (Discrete→isSet)
open import Data.List.Properties                 using (map-++-commute)
open import Data.Bool                            using (Bool; true; false)
open import Data.Empty                           using (⊥; ⊥-elim)
open import Data.Sum                             using (_⊎_; inj₁; inj₂)
open import Function                             using (_∘_; id)
open import Level
```

## Basics

```
variable
  ℓ ℓ′ ℓₙ : Level
  𝔸 X     : Type₀

data _∈_ {ℓ : Level} {A : Type ℓ} (x : A) : List A → Type ℓ where
  here  : x ∈ [ x ]
  there : (y : A) (xs : List A) → x ∈ xs → x ∈ (y ∷ xs)
```

```
IsDecidable : Type ℓ → Type ℓ
IsDecidable A = A ⊎ (A → ⊥)

HasDecidableEquality : Type ℓ → Type ℓ
HasDecidableEquality A = (x y : A) → IsDecidable (x ≡ y)
```

## Binary transposition on `𝔸`

```
module Transposition (𝔸 : Type ℓ) (_≈_ : HasDecidableEquality 𝔸) where

  ⦅_↔_⦆[_] : 𝔸 → 𝔸 → 𝔸 → 𝔸
  ⦅ a ↔ b ⦆[ c ] with a ≈ c | b ≈ c
  ⦅ a ↔ b ⦆[ c ] | inj₁ _   | _       = b
  ⦅ a ↔ b ⦆[ c ] | inj₂ _   | inj₁ y  = a
  ⦅ a ↔ b ⦆[ c ] | inj₂ _   | inj₂ ¬y = c
```

## Axiomatisation of swappings

```
module SwapActions (𝔸 : Type ℓₙ) (_≈_ : HasDecidableEquality 𝔸) (A : Type ℓ′) where

  open Transposition 𝔸 _≈_

  IsSwap : (𝔸 → 𝔸 → A → A) → Type (ℓ-max ℓₙ ℓ′)
  IsSwap swap = IsSwapId × IsSwapInv × IsSwapEquivar
    where
      IsSwapId      = (a   : 𝔸) (x : A) → swap a a x ≡ x
      IsSwapInv     = (a b : 𝔸) (x : A) → (swap a b ∘ swap a b) x ≡ x
      IsSwapEquivar = (a b c d : 𝔸) (x : A)
                    → (swap a b ∘ swap c d) x
                      ≡ (swap (⦅ a ↔ b ⦆[ c ]) (⦅ a ↔ b ⦆[ d ]) (swap a b x))

  Swap : Type (ℓ-max ℓₙ ℓ′)
  Swap = Σ (𝔸 → 𝔸 → A → A) IsSwap

  _§_ : Swap → 𝔸 × 𝔸 → A → A
  (sw , _) § (a , b) = sw a b
```

## The notion of supporting

```
module Support (𝔸 : Type ℓ) (_≈_ : HasDecidableEquality 𝔸) (X : Type ℓ′) where

  open Transposition 𝔸 _≈_
  open SwapActions   𝔸 _≈_ X

  supports : Swap → List 𝔸 → X → Type (ℓ-max ℓ ℓ′)
  supports sw as x =
    (a b : 𝔸) → (a ∈ as → ⊥) → (b ∈ as → ⊥) → (sw § (a , b)) x ≡ x
```

## Definition of a category

```
record RawCategory (ℓ₀ ℓ₁ : Level) : Type (ℓ-suc (ℓ₀ ⊔ ℓ₁)) where
  field
    Obj   : Type ℓ₀
    Arr   : Obj → Obj → Type ℓ₁
    𝟏     : (A : Obj) → Arr A A
    _<<<_ : {A B C : Obj} → Arr B C → Arr A B → Arr A C

record Category (ℓ₀ ℓ₁ : Level) : Type (ℓ-suc (ℓ₀ ⊔ ℓ₁)) where
  field
    raw : RawCategory ℓ₀ ℓ₁

  open RawCategory raw using (Obj; Arr; _<<<_; 𝟏)

  field
    id-l  : (A B : Obj) → (f : Arr A B) → (𝟏 B) <<< f     ≡ f            
    id-r  : (A B : Obj) → (f : Arr A B) → f     <<< (𝟏 A) ≡ f
    assoc : (A B C D : Obj) (h : Arr C D) (g : Arr B C) (f : Arr A B)
          → (h <<< g) <<< f ≡ h <<< (g <<< f)
```

## Definition of nominal sets

```
module Nominal (𝔸 : Type ℓₙ) (_≈_ : HasDecidableEquality 𝔸) where

  open Transposition 𝔸 _≈_


  IsNom : Type ℓ → Type (ℓ-max ℓₙ ℓ)
  IsNom X = Σ[ sw ∈ Swap ] ((x : X) → Σ[ as ∈ (List 𝔸) ] (supports sw as x))
    where
      open SwapActions 𝔸 _≈_ X
      open Support     𝔸 _≈_ X

  NomSet : (ℓ : Level) → Type (ℓₙ ⊔ suc ℓ)
  NomSet ℓ = Σ[ X ∈ (Type ℓ) ] (IsNom X × isSet X)

  ∣_∣ : NomSet ℓ → Type ℓ
  ∣ (A , _) ∣ = A

  swap-of : (A : NomSet ℓ) → 𝔸 × 𝔸 → ∣ A ∣ → ∣ A ∣
  swap-of (A , ((sw , supp) , _)) (a , b) = sw § (a , b)
    where
      open SwapActions 𝔸 _≈_ A

  [_]-set : (A : NomSet ℓ) → isSet ∣ A ∣
  [ _ , _ , is-set ]-set = is-set

  syntax swap-of A p x = p ⇔[ A ] x

  _─e→_ : NomSet ℓ → NomSet ℓ → Type (ℓ-max ℓₙ ℓ)
  _─e→_ {ℓ = ℓ} A B =
    Σ[ f ∈ (∣ A ∣ → ∣ B ∣) ]
      ((a b : 𝔸) → (x : ∣ A ∣) →
        (a , b) ⇔[ B ] (f x) ≡ f ((a , b) ⇔[ A ] x) )

  fn-ext : {A : Type ℓ} {B : A → Type ℓ′}
         → (f g : (x : A) → B x) → ((x : A) → f x ≡ g x) → f ≡ g
  fn-ext f g f~g i x = f~g x i

  to-subtype-≡ : {A : Type ℓ} {B : A → Type ℓ′}
               → (p q : Σ A B)
               → ((x : A) → isProp (B x))
               → proj₁ p ≡ proj₁ q → p ≡ q
  to-subtype-≡ _ _ B-prop eq =  ΣProp≡ B-prop eq 

  eqv-eq : (A B : NomSet ℓ) (f g : A ─e→ B)
         → (proj₁ f ≡ proj₁ g)
         → f ≡ g
  eqv-eq A B f g eq = to-subtype-≡ f g NTS eq
    where
      NTS : (x : ∣ A ∣ → ∣ B ∣) → isProp ((a b : 𝔸) (x₁ : ∣ A ∣) → swap-of B (a , b) (x x₁) ≡ x (swap-of A (a , b) x₁))
      NTS x = propPi (λ a → propPi λ b → propPi λ y → [ B ]-set (swap-of B (a , b) (x y)) (x (swap-of A (a , b) y)))
```

## Nominal sets form a category

```
  ide : (A : NomSet ℓ) → A ─e→ A
  ide A = id , id-equivariant
    where
      open SwapActions 𝔸 _≈_ ∣ A ∣

      id-equivariant : (a b : 𝔸) (x : ∣ A ∣)
                     → (a , b) ⇔[ A ] (id x) ≡ id ((a , b) ⇔[ A ] x)
      id-equivariant _ _ _ = refl

  eqv-comp : (A B C : NomSet ℓ) → B ─e→ C → A ─e→ B → A ─e→ C
  eqv-comp A B C (g , g-eqv) (f , f-eqv) = g ∘ f , g∘f-eqv
    where
      g∘f-eqv : (a b : 𝔸) (x : ∣ A ∣) → (a , b) ⇔[ C ] (g (f x)) ≡ g (f ((a , b) ⇔[ A ] x))
      g∘f-eqv a b x =
        (a , b) ⇔[ C ] (g (f x))  ≡⟨ g-eqv a b (f x)      ⟩
        g ((a , b) ⇔[ B ] (f x))  ≡⟨ cong g (f-eqv a b x) ⟩
        g (f ((a , b) ⇔[ A ] x))  ∎

  RawNom : (ℓ : Level) → RawCategory (ℓ-max ℓₙ (ℓ-suc ℓ)) (ℓ-max ℓₙ ℓ)
  RawNom ℓ = record
    { Obj   = NomSet ℓ
    ; Arr   = _─e→_
    ; 𝟏     = ide
    ; _<<<_ =  λ {A B C} g f → eqv-comp A B C g f
    }

  ∘-assoc : (A B C D : Type ℓ) (h : C → D) (g : B → C) (f : A → B)
          → (h ∘ g) ∘ f ≡ h ∘ (g ∘ f)
  ∘-assoc A B C D h g f = refl

  Nom : (ℓ : Level) → Category (ℓ-max ℓₙ (ℓ-suc ℓ)) (ℓ-max ℓₙ ℓ)
  Nom ℓ =
    record { raw = RawNom ℓ ; id-l = id-l ; id-r = id-r ; assoc = assoc }
    where
      id-l : (A B : NomSet ℓ) (f : A ─e→ B) → eqv-comp A B B (ide B) f ≡ f
      id-l A B f = eqv-eq A B (eqv-comp A B B (ide B) f) f (fn-ext (proj₁ (eqv-comp A B B (ide B) f)) (proj₁ f) (λ _ → refl))

      id-r : (A B : NomSet ℓ) (f : A ─e→ B) → eqv-comp A A B f (ide A) ≡ f
      id-r A B f = eqv-eq A B (eqv-comp A A B f (ide A)) f (fn-ext (proj₁ (eqv-comp A A B f (ide A))) (proj₁ f) (λ _ → refl))

      assoc : (A B C D : NomSet ℓ) (h : C ─e→ D) (g : B ─e→ C) (f : A ─e→ B)
            → eqv-comp A B D (eqv-comp B C D h g) f ≡ eqv-comp A C D h (eqv-comp A B C g f)
      assoc A B C D h g f =
        eqv-eq A D ((eqv-comp A B D (eqv-comp B C D h g) f)) (eqv-comp A C D h (eqv-comp A B C g f))  NTS
          where
            NTS : proj₁ (eqv-comp A B D (eqv-comp B C D h g) f) ≡ proj₁ (eqv-comp A C D h (eqv-comp A B C g f))
            NTS = ∘-assoc (∣ A ∣) (∣ B ∣) (∣ C ∣) (∣ D ∣) (proj₁ h) (proj₁ g) (proj₁ f)
```
